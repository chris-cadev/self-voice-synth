import pyaudio
import random
import wave
import os

p = pyaudio.PyAudio()

def get_audio_devices():
    """Print a list of available audio devices and their supported sample rates."""
    info = p.get_host_api_info_by_index(0)
    numdevices = info.get('deviceCount')
    for i in range(0, numdevices):
        if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            device_name = p.get_device_info_by_host_api_device_index(
                0, i
            ).get('name')
            print("{}: - {}".format(i, device_name))

def select_audio_device():
    """Ask the user to choose an audio device and return its device index and sample rate."""
    device_index = int(input("Enter the index of the audio device you want to use: "))
    p = pyaudio.PyAudio()
    device_info = p.get_device_info_by_host_api_device_index(0, device_index)
    fs = int(device_info['defaultSampleRate'])
    return device_index, fs

def record_audio(device_index, fs, duration, phrases):
    """Record audio for the specified number of seconds using the given device and sample rate, and return the recorded audio frames."""
    # Choose a random phrase to say
    phrase = random.choice(phrases)
    print("Say this phrase: {}".format(phrase))

    # Open an audio stream using the selected device
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16, channels=1, rate=fs, input=True, input_device_index=device_index)

    # Record audio for the specified number of seconds
    print("Recording...")
    frames = []
    chunk = 1
    for _ in range(int(fs / chunk * duration)):
        data = stream.read(chunk)
        frames.append(data)

    # Stop the stream and close it
    stream.stop_stream()
    stream.close()
    return frames

def save_audio(frames, filename):
    """Save the given audio frames to a wave file with the given filename."""

    dirname = os.path.dirname(filename)
    if dirname and not os.path.exists(dirname):
        os.makedirs(dirname)

    with wave.open(filename, 'w') as wav_file:
        wav_file.setnchannels(1)
        wav_file.setsampwidth(2)
        wav_file.setframerate(fs)
        wav_file.writeframes(b''.join(frames))

# Set the parameters for the audio recording
duration = 5  # Recording duration in seconds
WAVE_OUTPUT_FILENAME = "recordings/{}.wav"

# Create a list of random phrases to say
phrases = ["The early bird catches the worm.",
           "Practice makes perfect.",
           "A picture is worth a thousand words.",
           "Rome wasn't built in a day.",
           "Don't judge a book by its cover.",
           "The grass is always greener on the other side.",
           "Good things come to those who wait.",
           "Actions speak louder than words.",
           "Two heads are better than one.",
           "Where there's smoke, there's fire."]

# Print a list of available audio devices and their supported sample rates
get_audio_devices()

# Ask the user to choose an audio device
device_index, fs = select_audio_device()

# Record 10 audio samples in a row
for i in range(10):
    # Record audio for the specified number of seconds
    frames = record_audio(device_index, fs, duration, phrases)

    # Save the recording to a wave file
    save_audio(frames, WAVE_OUTPUT_FILENAME.format(i))
    print("Recording saved to {}".format(WAVE_OUTPUT_FILENAME.format(i)))


p.terminate()