import os

import speech_recognition as sr

# Set the path to the directory containing the audio files
audio_dir = "recordings"

# Iterate through all the audio files in the directory
for filename in os.listdir(audio_dir):
    # Check if the file is an audio file
    if filename.endswith(".wav"):
        # Set the path to the audio file
        audio_path = os.path.join(audio_dir, filename)

        # Initialize the recognizer
        r = sr.Recognizer()

        # Read the audio file
        with sr.AudioFile(audio_path) as source:
            audio = r.record(source)

        # Convert the audio file to text
        text = r.recognize_google(audio)

        # Write the text to a file
        with open(f"{audio_dir}/{filename}.txt", "w") as f:
            f.write(text)
