import pyaudio

p = pyaudio.PyAudio()

def get_audio_devices():
    """Print a list of available audio devices and their supported sample rates."""
    info = p.get_host_api_info_by_index(0)
    numdevices = info.get('deviceCount')
    for i in range(0, numdevices):
        if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            device_name = p.get_device_info_by_host_api_device_index(
                0, i
            ).get('name')
            print("{}: - {}".format(i, device_name))

def select_audio_device():
    """Ask the user to choose an audio device and return its device index and sample rate."""
    device_index = int(input("Enter the index of the audio device you want to use: "))
    p = pyaudio.PyAudio()
    device_info = p.get_device_info_by_host_api_device_index(0, device_index)
    fs = int(device_info['defaultSampleRate'])
    return device_index, fs

# Print a list of available audio devices and their supported sample rates
get_audio_devices()

# Ask the user to choose an audio device
device_index, fs = select_audio_device()

print(f"sample rate: {fs}")


p.terminate()